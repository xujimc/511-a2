package com.example.assignment2

sealed class Routes(val route: String) {
    object FoodPagePortrait : Routes("FoodPagePortrait")
    object ListPagePortrait : Routes("ListPagePortrait")
    object FoodPageLandscape : Routes("FoodPageLandscape")
    object ListPageLandscape : Routes("ListPageLandscape")
}
