package com.example.assignment2

import android.app.SearchManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.assignment2.ui.theme.Assignment2Theme
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel


class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Assignment2Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val windowSize = calculateWindowSizeClass(this)
                    if(windowSize.widthSizeClass == WindowWidthSizeClass.Medium){
                        FoodPageLandscape(viewModel(), windowSize)
                    }
                    else{
                        FoodPagePortrait(viewModel(), windowSize)
                    }
                }
            }
        }
    }

    @Composable
    fun ImageOfFood(theFood: String, windowSize: WindowWidthSizeClass){
        val context = LocalContext.current
        var image = R.drawable.food
        var contentDescription = stringResource(R.string.icon_of_suggested_food)
        if(theFood == stringResource(R.string.pizza)) {
            image = R.drawable.pizza
            contentDescription = stringResource(R.string.pizza)
        }
        if(theFood == stringResource(R.string.burger)) {
            image = R.drawable.burger
            contentDescription = stringResource(R.string.burger)
        }
        if(theFood == stringResource(R.string.chicken)) {
            image = R.drawable.chicken
            contentDescription = stringResource(R.string.chicken)
        }
        if(theFood == stringResource(R.string.sushi)) {
            image = R.drawable.sushi
            contentDescription = stringResource(R.string.sushi)
        }
        if(theFood == stringResource(R.string.sandwich)) {
            image = R.drawable.sandwich
            contentDescription = stringResource(R.string.sandwich)
        }
        Image(
            painter = painterResource(image),
            contentDescription = contentDescription,
            modifier = Modifier
                .size(400.dp)
                .clickable(onClick = {
                    val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=$theFood"))
                    context.startActivity(mapIntent)
                })
        )
    }
    @Composable
    fun FoodPageLandscape(appVM: AppVM = viewModel(), windowSize: WindowSizeClass){
        if(!appVM.isPopulated){
            val starterItems: Array<String> = resources.getStringArray(R.array.starter_items)
            for(starterItem in starterItems){
                appVM.addItem(starterItem)
            }
            appVM.setIsPopulated()
        }
        val context = LocalContext.current
        if(appVM.currentFood == "")appVM.getRandomFood(appVM.foodList)
        val navController = rememberNavController()
        NavHost(navController = navController, startDestination = Routes.FoodPageLandscape.route )
        {
            composable(Routes.FoodPageLandscape.route) {

                Column(){
                    Text(stringResource(R.string.you_should_eat), fontSize = 40.sp)
                    Text(appVM.currentFood, fontSize = 40.sp)
                    Row(){
                        ImageOfFood(appVM.currentFood, windowSize.widthSizeClass)
                        Column{
                            Button(onClick = {appVM.getRandomFood(appVM.foodList)}) {Text("Re-roll")}
                            Button(onClick = {
                                val intent = Intent(Intent.ACTION_WEB_SEARCH).apply{
                                    putExtra(SearchManager.QUERY, appVM.currentFood)
                                }
                                context.startActivity(intent)
                            }) {Text(stringResource(R.string.find_food))}
                            Button(onClick = {
                                navController.navigate(Routes.ListPageLandscape.route)
                            }) {Text(stringResource(R.string.change_list))}
                        }
                    }
                }
            }
            composable(Routes.ListPageLandscape.route){
                ListPageLandscape(navController = navController, appVM)
            }
        }

    }
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun ListPageLandscape(navController: NavController, appVM: AppVM){
        var textInput by rememberSaveable { mutableStateOf("") }

        Column(){
            Row(){
                val onTextChange = { text : String -> textInput = text }
                TextField(value = textInput, onValueChange = onTextChange)
                Button(onClick = {
                    if(textInput.isNotEmpty()){
                        appVM.addItem(textInput)
                    }
                    navController.navigate(Routes.FoodPageLandscape.route){
                        popUpTo(Routes.FoodPageLandscape.route)
                    }
                }) {Text(stringResource(R.string.save_changes))}
                Button(onClick = {
                    if(textInput.isNotEmpty()){
                        appVM.addItem(textInput)
                        textInput = ""
                    }
                }) {Text(stringResource(R.string.add_item))}
            }
            repeat(appVM.amountOfFood){
                Button(onClick = {
                    appVM.removeItem(appVM.foodList[it])
                }){
                    Text(appVM.foodList[it])
                }
            }
        }
    }




        @Composable
        fun FoodPagePortrait(appVM: AppVM = viewModel(), windowSize: WindowSizeClass){
        if(!appVM.isPopulated){
            val starterItems: Array<String> = resources.getStringArray(R.array.starter_items)
            for(starterItem in starterItems){
                appVM.addItem(starterItem)
            }
            appVM.setIsPopulated()
        }
        val context = LocalContext.current
        if(appVM.currentFood == "")appVM.getRandomFood(appVM.foodList)
        val navController = rememberNavController()
        NavHost(navController = navController, startDestination = Routes.FoodPagePortrait.route )
        {
            composable(Routes.FoodPagePortrait.route) {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ){
                    Text(stringResource(R.string.you_should_eat), fontSize = 40.sp)
                    ImageOfFood(appVM.currentFood, windowSize.widthSizeClass)
                    Text(appVM.currentFood, fontSize = 40.sp)
                    Row{
                        Button(onClick = {appVM.getRandomFood(appVM.foodList)}) {Text("Re-roll")}
                        Button(onClick = {
                            val intent = Intent(Intent.ACTION_WEB_SEARCH).apply{
                                putExtra(SearchManager.QUERY, appVM.currentFood)
                            }
                            context.startActivity(intent)
                        }) {Text(stringResource(R.string.find_food))}
                        Button(onClick = {
                            navController.navigate(Routes.ListPagePortrait.route)
                        }) {Text(stringResource(R.string.change_list))}
                    }
                }
            }

            composable(Routes.ListPagePortrait.route){
                ListPagePortrait(navController = navController, appVM)
            }
        }

    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun ListPagePortrait(navController: NavController, appVM: AppVM){
        var textInput by rememberSaveable { mutableStateOf("") }
        Column(horizontalAlignment = Alignment.CenterHorizontally){
            Row{
                val onTextChange = { text : String -> textInput = text }
                TextField(value = textInput, onValueChange = onTextChange)
                Column{
                    Button(onClick = {
                        if(textInput.isNotEmpty()){
                            appVM.addItem(textInput)
                        }
                        navController.navigate(Routes.FoodPagePortrait.route){
                            popUpTo(Routes.FoodPagePortrait.route)
                        }
                    }) {Text(stringResource(R.string.save_changes))}
                    Button(onClick = {
                        if(textInput.isNotEmpty()){
                            appVM.addItem(textInput)
                            textInput = ""
                        }
                    }) {Text(stringResource(R.string.add_item))}
                }
            }
            repeat(appVM.amountOfFood){
                Button(onClick = {
                    appVM.removeItem(appVM.foodList[it])
                }){
                    Text(appVM.foodList[it])
                }
            }
        }
    }

    @Preview(showBackground = true, showSystemUi = true)
    @Composable
    fun GreetingPreview() {
        Assignment2Theme {
            //no longer works
        }
    }
}