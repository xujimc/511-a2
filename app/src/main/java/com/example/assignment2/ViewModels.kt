package com.example.assignment2


import androidx.compose.runtime.*
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class AppVM() : ViewModel(){
    var isPopulated by mutableStateOf(false)
    fun setIsPopulated(){
        isPopulated = true
    }

    val foodList = ArrayList<String>()

    //this state variable is a control, i will make sure it updates whenever the
    //data array is changed, and i will access it to make sure it is subscribed to
    var amountOfFood by mutableStateOf(foodList.count())
    fun addItem(item: String){
        if(!foodList.contains(item)){
            foodList.add(item)
            amountOfFood = foodList.count()
        }
    }
    fun removeItem(item: String){
        if(foodList.contains(item)){
            foodList.remove(item)
            amountOfFood = foodList.count()
        }
    }

    var currentFood by mutableStateOf("")
    fun getRandomFood(foodList: ArrayList<String>){
        if(foodList.isEmpty()) {
            currentFood = "Empty List"
            return
        }
        if(foodList.count() == 1) {
            currentFood = foodList[0]
            return
        }
        var output = ""
        while(output == currentFood || output == "") {
            val rng = Random.nextInt(foodList.count())
            output = foodList[rng]
        }
        currentFood = output
    }
}